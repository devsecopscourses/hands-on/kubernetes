# Kubernetes Application Deployment Tutorial

This tutorial guides you through the process of deploying an application in a Kubernetes cluster. We'll use a Deployment to manage the application instances and a Service to expose the application.

## Creating a Deployment

1. **Create a Deployment Using kubectl**:

You can create a deployment directly using `kubectl`. For example, to deploy a simple nginx application, use the following command:

```bash
  kubectl create deployment nginx-deployment --image=nginx
```

This command creates a deployment named nginx-deployment using the nginx image.

2. **Create a Deployment Using a YAML File**:

Alternatively, create a file named `nginx-deployment.yaml` with the following content:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
```

### Explanation of the YAML Manifest

YAML file for Kubernetes is a manifest that describes the resources and their configurations that you want to deploy in the cluster. Here's a detailed breakdown of the nginx-pod.yaml file:

- `apiVersion: apps/v1`:
  - This specifies the version of the Kubernetes API you're using to create the object. `apps/v1` is the version for Deployment resources, which are part of the "apps" group in the Kubernetes API.
- `kind: Deployment`:
  - This indicates the kind of Kubernetes object you want to create. Here, `Deployment` is the object type, which is used for deploying applications and managing a set of replicated pods.
- `metadata`:
  - This section contains metadata about the Deployment.
  - `name: nginx-deployment`: This is the name of the Deployment. It's how you'll refer to the Deployment within the Kubernetes cluster.
- `spec`:
  - This section specifies the desired state of the Deployment.
  - `replicas: 3`: This tells Kubernetes how many copies of the pod should be running. In this case, it's set to 3.
  - `selector`:
    - This field tells the Deployment which pods it should manage.
    - `matchLabels`:
      - `app: nginx`: This selector matches any pods with labels that match this key-value pair. It's used to identify the set of pods that belong to this Deployment.
- `template`:
  - This defines the pod template that the Deployment will use to create new pods.
  - `metadata`:
    - `labels`:
      - `app: nginx`: These are the labels applied to the pods created by this Deployment. They should match the selector in the Deployment spec.
  - `spec`:
    - This specifies the configuration of the pods.
    - `containers`:
      - A list of container specifications.
      - `name: nginx`: The name of the container within the pod.
      - `image: nginx`: The Docker image to use for the container. In this case, it's using the official nginx image from Docker Hub.

Apply the file using kubectl:

```bash
kubectl apply -f nginx-deployment.yaml
```

## Retrieving Deployment Information

After deploying your application, it's important to know how to view its details and status. Kubernetes provides `kubectl describe` and `kubectl get` commands for this purpose.

### Using kubectl get

The `kubectl get` command allows you to list resources. To see your deployments, use:

```bash
kubectl get deployments
```

To get more detailed information about a specific deployment, use:

```bash
kubectl get deployment nginx-deployment -o wide
```

The `-o wide` option provides additional details like the number of replicas, image version, and more.

### Using `kubectl describe`

For a more detailed view, use `kubectl describe`. This command shows a comprehensive overview of the deployment, including events, which can be useful for debugging.

To describe your deployment, use:

```bash
kubectl describe deployment nginx-deployment
```

This command provides detailed information about the deployment, including:

- The number of replicas and their statuses.
- Pod selector details.
- Strategy used for updating the pods.
- Events related to the deployment process.

### Example Use Case

Imagine you've updated your deployment, but the new pods are not starting. You can use `kubectl describe` to check the events and logs associated with the deployment to identify the issue, such as image pull errors or resource constraints.

## Updating the Application

To update the application, you can change the image used by the deployment:

```bash
kubectl set image deployment/nginx-deployment nginx=nginx:1.16.1
```

This command updates the nginx deployment to use a specific version of the nginx image.

## Monitoring a Deployment

To check the status of a Deployment:

```bash
kubectl rollout status deployment/nginx-deployment
```

## Rolling Back a Deployment

If something goes wrong, Kubernetes allows you to roll back to a previous state of the Deployment.

```bash
kubectl rollout undo deployment/nginx-deployment
```

## Scaling a Deployment

You can scale a Deployment by updating the number of replicas.

```bash
kubectl scale deployment nginx-deployment --replicas=5
```

## Cleaning Up

To delete the deployment and service, use:

```bash
kubectl delete deployment nginx-deployment
```
