# Deploying a Pod in Kubernetes

This guide covers how to deploy a pod in Kubernetes using both the command line interface (CLI) and a YAML manifest file, as well as how to access it.

## Deployment via CLI

1. **Create a Pod with kubectl**:
   You can create a pod directly using `kubectl`. For example, to deploy a simple nginx pod, use the following command:

```bash
  kubectl run nginx-pod --image=nginx
```

This command creates a pod named nginx-pod using the nginx image.

### Explanation of the `kubectl run` Command

The `kubectl run` command is used to create and run a specific instance (pod) in a Kubernetes cluster. Here's the basic structure of this command:

```bash
  kubectl run <pod-name> --image=<image-name>
```

- `<pod-name>`: This is the name you assign to your pod. In the example nginx-pod, this is the name given to the pod.
- `--image=<image-name>`: Specifies the Docker image to use for the container in the pod. For example, nginx indicates that the pod will use the official nginx image from Docker Hub.

This command creates a pod with the given specifications. It's convenient for quick and simple deployments but offers less customization than using a YAML file.

## Deployment via YAML Manifest

1. Use the file named `nginx-pod.yaml` in manifest directory with the following content:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
spec:
  containers:
  - name: nginx
    image: nginx
```

2. Deploy the Pod:

Use `kubectl` to apply the YAML file:

```bash
kubectl apply -f nginx-pod.yaml
```

### Explanation of the YAML Manifest

A YAML file for Kubernetes is a manifest that describes the resources and their configurations that you want to deploy in the cluster. Here's a detailed breakdown of the nginx-pod.yaml file:

- `apiVersion`: Specifies the version of the Kubernetes API used to create the object. Here, v1 is the version for pods.
- `kind`: The type of Kubernetes object you want to create. In this case, it's a Pod.
- `metadata`:
  - `name`: The name of the pod, here nginx-pod.
- `spec`: Specifies the characteristics of the pod.
  - `containers`: A list of containers that the pod should run.
    - `name`: The name of the container, here nginx.
    - `image`: The Docker image to use for this container, here the official nginx image.

Using a YAML file gives you more control over configurations and allows you to set more complex parameters for your Kubernetes resources.

## Accessing the Pod

1. Check the Pod Status:

Before accessing the pod, make sure it is up and running:

```bash
kubectl get pods
```

2. Access the Pod via Port Forwarding:

If you want to access the pod via a web browser or HTTP client, you can use port forwarding. For example, to forward port 80 of the pod to port 8080 on your local machine:

```bash
kubectl port-forward pod/nginx-pod 8080:80
```

kubectl port-forward pod/nginx-pod 8080:80

3. Enter the Pod:

To enter the pod and interact with its shell, use:

```bash
kubectl exec -it nginx-pod -- /bin/bash
```

This will allow you to run commands inside the nginx container.

These steps will help you deploy and access a simple pod in Kubernetes. Ensure your Kubernetes environment (like Minikube) is running and kubectl is configured to communicate with your cluster.

## Viewing Pod Logs

To troubleshoot issues or understand the behavior of your application, you might need to view the logs of a pod. Kubernetes provides an easy way to access these logs using `kubectl`.

1. **View Logs of a Pod**:

Use the following command to view the logs of a specific pod:

```bash
kubectl logs nginx-pod
```

This command will display the logs from the nginx-pod. If your pod contains multiple containers, you'll need to specify the container name.

1. **View Logs from a Specific Container in a Pod**:
If your pod has more than one container, specify the container name using the -c flag:

```bash
kubectl logs nginx-pod -c <container-name>
```

Replace `<container-name>` with the name of the container you want to view logs from.

2. **Stream Logs in Real-Time**:
To follow the log output in real-time, use the -f flag:

```bash
kubectl logs -f nginx-pod
```

This is particularly useful for monitoring ongoing activities or issues within the pod.

3. **View Previous Logs of a Crashed Container**:

If a container in the pod has crashed, you can view the logs from its previous instance with the `--previous` flag:

```bash
kubectl logs nginx-pod --previous
```

This command is helpful for debugging crash-related issues.

These commands provide valuable insights into the functioning and issues of your application running in Kubernetes pods.

## Deleting the Pod

**Delete the Pod Using CLI**:

To delete the pod, use the following command:

```bash
  kubectl delete pod nginx-pod
```

This command will remove the pod named nginx-pod from your Kubernetes cluster.

**Delete the Pod Using a YAML File**:

If you have deployed the pod using a YAML file, you can also delete it using the same file. For example, if you used nginx-pod.yaml for deployment, use the following command to delete the pod:

```bash
kubectl delete -f nginx-pod.yaml
```

This command will delete the resources defined in the nginx-pod.yaml file from your Kubernetes cluster.
