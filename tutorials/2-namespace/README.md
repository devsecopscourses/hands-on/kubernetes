# Kubernetes Namespaces Tutorial

This tutorial provides a comprehensive guide on how to manage Kubernetes namespaces. Namespaces in Kubernetes are used to organize clusters into virtual sub-clusters, providing a way to divide cluster resources between multiple users.

## Table of Contents

- [Kubernetes Namespaces Tutorial](#kubernetes-namespaces-tutorial)
  - [Table of Contents](#table-of-contents)
  - [Creating a Namespace](#creating-a-namespace)
  - [Listing Namespaces](#listing-namespaces)
  - [Deleting a Namespace](#deleting-a-namespace)
  - [Working with Namespaces](#working-with-namespaces)

## Creating a Namespace

1. **Create a Namespace Using kubectl**:

To create a new namespace, use the following command:

```bash
  kubectl create namespace <namespace-name>
```

2. **Create a Namespace Using a YAML File:**

Alternatively, you can create a namespace using a YAML file. Use the file named my-namespace.yaml in manifest directory with the following content:

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: my-namespace
```

Apply the file using kubectl:

```bash
  kubectl apply -f my-namespace.yaml
```

## Listing Namespaces

To list all the namespaces in your cluster, use the command:

```bash
  kubectl get namespaces
```

This command will display all namespaces, including the default ones provided by Kubernetes.

## Deleting a Namespace

1. **Delete a Namespace Using kubectl:**

To delete a namespace, use the following command:

```bash
  kubectl delete namespace <namespace-name>
```

Replace `<namespace-name>` with the name of the namespace you want to delete.

2. **Delete a Namespace Using a YAML File:**

If you have a YAML file for the namespace, you can delete it using the file:

```bash
  kubectl delete -f my-namespace.yaml
```

## Working with Namespaces

- **Setting the Namespace for a kubectl Command:**

To run a `kubectl` command in a specific namespace, use the `--namespace` flag. For example:

```bash
  kubectl get pods --namespace=my-namespace
```

- **Setting a Default Namespace for kubectl:**

You can set a default namespace for your `kubectl` commands. This way, you don't have to specify the `--namespace` flag every time. Use the following command to set a default namespace:

```bash
  kubectl config set-context --current --namespace=my-namespace
```

Replace `my-namespace` with your namespace name.
