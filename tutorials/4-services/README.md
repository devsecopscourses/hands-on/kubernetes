# Kubernetes Services Tutorial

This tutorial will guide you through the process of creating and managing Kubernetes Services. Services in Kubernetes are an essential component for connecting applications, both internally within the cluster and externally. We'll explore different types of services and how to use them.

## Understanding Kubernetes Services

Kubernetes Services provide a way to expose an application running on a set of Pods as a network service. Unlike individual Pods, which can be ephemeral, a Service has a stable IP address and DNS name entry, ensuring that the service endpoint remains consistent regardless of changes in the underlying Pods.

## Types of Services in Kubernetes

1. **ClusterIP**: Default type, exposes the service on an internal IP in the cluster. Accessible only within the cluster.
2. **NodePort**: Exposes the service on each Node’s IP at a specified port. Accessible externally.
3. **LoadBalancer**: Exposes the service externally using a cloud provider’s load balancer.
4. **ExternalName**: Maps the service to a specified externalName field by returning a CNAME record with its value.

## Creating a Service

### 1. Creating a ClusterIP Service

- **Use Case**: Internal communication within the cluster.
- **Command**:

```bash
kubectl expose deployment nginx-deployment --type=ClusterIP --name=my-internal-service
```

- **YAML Example**:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-internal-service
spec:
  type: ClusterIP
  selector:
    app: nginx
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```

- `apiVersion: v1`: Specifies the version of the Kubernetes API used to create the object.
- `kind: Service`: Indicates that the object being created is a service.
- `metadata`: Contains metadata about the service.
  - `name: my-internal-service`: The name of the service.
- `spec`: Specifies the desired state of the service.
  - `type: ClusterIP`: The type of service, here ClusterIP, which is only accessible within the cluster.
  - `selector`: Selects the pods to which the service should be linked.
    - `app: nginx`: Selects pods with the label `app=nginx`.
  - `ports`: Configures the ports on which the service is exposed.
    - `protocol: TCP`: Uses the TCP protocol.
      - `port: 80`: The service is accessible on port 80.
      - `targetPort: 80`: Directs traffic to port 80 of the pod.

### 2. Creating a NodePort Service

- **Use Case**: External access to your services.
- **Command**:

```bash
kubectl expose deployment nginx-deployment --type=NodePort --name=my-nodeport-service
```

- **YAML Example**:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-nodeport-service
spec:
  type: NodePort
  selector:
    app: nginx
  ports:
    - protocol: TCP
      port: 80
      nodePort: 30007
```

- `type: NodePort`: This type of service exposes the service on a specific port of each node in the cluster.
- `nodePort: 30007`: Specifies the port on which the service is accessible from outside the cluster.

Note 1 : Kubernetes allocates ports from **30000 à 32767**
Note 2 : If you're using minikube, use `minikube service -all`

### 3. Creating a LoadBalancer Service

- **Use Case**: Exposing the service externally for production environments.
- **Command**:

```bash
kubectl expose deployment nginx-deployment --type=LoadBalancer --name=my-loadbalancer-service
```

- **YAML Example**:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-loadbalancer-service
spec:
  type: LoadBalancer
  selector:
    app: nginx
  ports:
    - protocol: TCP
      port: 80
```

- `type: LoadBalancer`: This type of service exposes the service via an external load balancer provided by a cloud provider.
- The rest of the configuration is similar to ClusterIP and NodePort services, with the `selector` to bind the service to pods and the `ports` configuration.

### 4. Creating an ExternalName Service

- **Use Case**: Accessing an external service.
- **Command**:

```bash
kubectl expose deployment nginx-deployment --type=LoadBalancer --name=my-loadbalancer-service
```

- **YAML Example**:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-externalname-service
spec:
  type: ExternalName
  externalName: my.external.service.com
  ports:
    - protocol: TCP
      port: 80
```

- `type: ExternalName`: This type of service allows mapping the service to an external DNS name.
- `externalName: my.external.service.com`: Specifies the external DNS name to which the service should be mapped.
- This type of service does not route traffic through a proxy but simply returns a CNAME record in DNS.

## Managing Services

- **Listing Services**:

```bash
kubectl get services
```

- **Describing a Service**:

```bash
kubectl describe service my-service-name
```

- **Deleting a Service**:

```bash
kubectl delete service my-service-name
```
