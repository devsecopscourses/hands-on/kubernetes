# Kubernetes Persistent Storage with Minikube

This guide walks through setting up persistent storage in Kubernetes using Minikube, including the creation and usage of a Persistent Volume (PV) and Persistent Volume Claim (PVC).

## Step 1: Preparing Persistent Storage in Minikube

Use Minikube's SSH to access the Minikube VM and create a directory for persistent data:

```bash
minikube ssh
sudo mkdir /mnt/data
sudo sh -c "echo 'Hello from Kubernetes storage' > /mnt/data/index.html"
```

## Step 2: Creating a Persistent Volume

Create a Persistent Volume (PV) to represent the storage. The following YAML defines a PV:

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: task-pv-volume
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/data"
```

Explanation:

- `storageClassName`: manual: Specifies the storage class. `manual` indicates that the volume is manually managed.
- `capacity`: The size of the storage, in this case, 10 Gigabytes.
- `accessModes`: ReadWriteOnce means the volume can be mounted as read-write by a single node.
- `hostPath`: Path to the storage on the host. `/mnt/data` is where data will be stored on the Minikube VM.

## Step 3: Verifying the Persistent Volume

To see the created PV:

```bash
kubectl get pv
```

## Step 4: Creating a Persistent Volume Claim

Create a PVC to request storage. The following YAML defines a PVC:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: task-pv-claim
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 3Gi
```

Explanation:

- The PVC requests 3Gi of storage.
- It uses the same `storageClassName` as the PV, ensuring they are compatible.
- The `accessModes` matches the PV.

Use `kubectl get pvc` and `kubectl describe pvc task-pv-claim` to view the status and details of the PVC.

## Step 5: Creating a Pod to Use the PVC

Create a Pod that uses the PVC. Here's the YAML for the Pod:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: task-pv-pod
spec:
  volumes:
    - name: task-pv-storage
      persistentVolumeClaim:
        claimName: task-pv-claim
  containers:
    - name: task-pv-container
      image: nginx
      ports:
        - containerPort: 80
          name: "http-server"
      volumeMounts:
        - mountPath: "/usr/share/nginx/html"
          name: task-pv-storage
```

Explanation:

- The Pod `task-pv-pod` has one container running Nginx.
- It mounts the PVC `task-pv-claim` at `/usr/share/nginx/html`, the default directory where Nginx serves files.

## Step 6: Verifying the Mounted Volume

Access the Pod and view the contents of the mounted volume:

```bash
kubectl exec -it task-pv-pod -- /bin/cat /usr/share/nginx/html/index.html
```

This command should display "Hello from Kubernetes storage", confirming that the persistent storage is correctly mounted and accessible by the Pod.
