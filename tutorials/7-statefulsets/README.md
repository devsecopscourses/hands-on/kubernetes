# Kubernetes PostgreSQL StatefulSet and Service Configuration

This guide explains the setup of a PostgreSQL StatefulSet and a headless Service in Kubernetes, including how it operates and the commands associated with deploying and managing it.

## Overview

The setup consists of two main components:

1. **Service**: A headless Service for stable networking.
2. **StatefulSet**: A StatefulSet to manage the PostgreSQL pods.

## Service Configuration

```yaml
apiVersion: v1
kind: Service
metadata:
  name: postgres
  labels:
    app: postgres
spec:
  ports:
    - name: postgres
      port: 5432
  clusterIP: None
  selector:
    app: postgres
```

Explanation:

- `Headless Service`: The clusterIP: None specification creates a headless service, which doesn't have a single IP and is used for service discovery.
- `Port`: Exposes PostgreSQL on port 5432.
- `Selector`: Matches pods labeled with app: postgres.

## Understanding Headless Service in Kubernetes

A headless service in Kubernetes is crucial for stateful applications like databases. It is defined with `clusterIP: None`, and here's how it differs and its importance:

### Key Characteristics of Headless Service

#### No Load Balancing or Single IP

- Unlike typical Kubernetes Services, a headless service does not have a cluster IP and does not perform load balancing.
- This type of service is used for direct pod-to-pod communication within a StatefulSet.

#### Service Discovery

- A headless service facilitates service discovery, allowing pods to directly discover and communicate with each other.
- DNS queries to a headless service return the IP addresses of the pods that are part of the service.

#### Role in StatefulSets

- Headless services are particularly beneficial in StatefulSets, where each pod has a stable identity and needs to be addressable individually.
- It is ideal for database clusters where specific pod instances may need to be accessed directly.

#### DNS and Headless Services

- Kubernetes assigns a unique DNS subdomain for each pod in the format: `[pod-name].[headless-service-name].[namespace].svc.cluster.local`.
- This DNS feature ensures that each pod can be reached individually through its unique address.

### Example: PostgreSQL Headless Service

In the provided PostgreSQL StatefulSet, the headless service named `postgres` allows each database pod to be directly reachable, facilitating individual pod management and communication.

### DNS Lookup Command

To see the DNS entries for the pods in the headless service:

```bash
kubectl run -i --tty --rm debug --image=azukiapp/dig --restart=Never -- dig postgres.default.svc.cluster.local
```

This command performs a DNS lookup for the postgres service, revealing the IP addresses associated with the pods.

Note: Wait to deploy the application before to run this command

## StatefulSet Configuration

The following YAML defines the StatefulSet for PostgreSQL:

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: postgres
```

- **apiVersion**: This specifies the version of the Kubernetes API you're using.
- **kind**: This tells Kubernetes that the resource type you want to create is a StatefulSet.
- **metadata**: This section provides metadata about the StatefulSet. In this case, it's named `postgres`.

```yaml
spec:
  selector:
    matchLabels:
      app: postgres
  serviceName: postgres
  replicas: 3
```

- **spec**: Specifies the desired state of the StatefulSet.
- **selector**: This selects the pods that belong to this StatefulSet, based on labels. Here, it selects pods with the label `app: postgres`.
- **serviceName**: Associates the pods with the `postgres` headless service for networking.
- **replicas**: Specifies that there should be 3 replicas of the pod.

```yaml
template:
  metadata:
    labels:
      app: postgres
  spec:
    initContainers:
      - name: postgres-init
        image: postgres:latest
        command:
        - bash
        - "-c"
        - |
          set -ex
          [[ `hostname` =~ -([0-9]+)$ ]] || exit 1
          ordinal=${BASH_REMATCH[1]}
          if [[ $ordinal -eq 0 ]]; then
            printf "I am the primary"
          else
            printf "I am a read-only replica"
          fi
```

- **template**: This defines the pod template. Each pod in the StatefulSet will be created based on this template.
- **initContainers**: These are containers that run before the main application container. In this case, it's used to run some initialization logic.

```yaml
containers:
        - name: postgres
          image: postgres:latest
          env:
            - name: POSTGRES_USER
              value: postgres
            - name: POSTGRES_PASSWORD
              value: postgres
            - name: POD_IP
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: status.podIP
```

- **containers**: This section defines the main container (in this case, PostgreSQL) to run in each pod.
- **image**: Specifies the Docker image to use for the container, here `postgres:latest`.
- **env**: Sets environment variables in the container. This is where you can set things like database user and password.

```yaml
ports:
- name: postgres
  containerPort: 5432
```

- **ports**: This section specifies the network ports that the container exposes.
- **containerPort**: 5432: PostgreSQL, by default, listens on port 5432. This configuration makes the port available to Kubernetes and other pods within the cluster.

```yaml
livenessProbe:
  exec:
    command:
      - "sh"
      - "-c"
      - "pg_isready --host $POD_IP"
  initialDelaySeconds: 30
  periodSeconds: 5
  timeoutSeconds: 5
```

- **livenessProbe**: This section tells Kubernetes how to decide if the PostgreSQL container is alive and running properly.
- **exec.command**: Executes a shell command inside the container. Here, `pg_isready --host $POD_IP` checks if PostgreSQL is ready to accept connections.
- **initialDelaySeconds**: 30: The liveness probe waits for 30 seconds after the container starts before performing the first check.
- **periodSeconds**: 5: After the initial delay, the liveness probe runs this check every 5 seconds.
- **timeoutSeconds**: 5: If the probe takes more than 5 seconds to complete, the system considers it failed.

```yaml
readinessProbe:
  exec:
    command:
      - "sh"
      - "-c"
      - "pg_isready --host $POD_IP"
  initialDelaySeconds: 5
  periodSeconds: 5
  timeoutSeconds: 1
```

- **readinessProbe**: Determines if the container is ready to start accepting traffic.
- The commands and checks are similar to the liveness probe, but the timings differ:
    - **initialDelaySeconds**: 5: Starts checking after 5 seconds of the container's startup.
    - **timeoutSeconds**: 1: If the readiness probe doesn’t complete in 1 second, it’s considered failed.

```yaml
volumeMounts:
- name: data
  mountPath: /var/lib/postgresql/data
```

- **Purpose**: This section of the container specification tells Kubernetes where to mount the volume inside the container.
- **name: data**: Refers to the name of the volume. This name is a reference to the volume defined in the `volumes` section of the Pod or, in this case, in the `volumeClaimTemplates` of the StatefulSet.
- **mountPath: /var/lib/postgresql/data**: The path inside the container where the volume will be mounted. For PostgreSQL, this path is the default directory where the database stores its data files. By mounting a volume here, you ensure that the database data persists across Pod restarts and deployments.

```yaml
volumeClaimTemplates:
- metadata:
    name: data
  spec:
    accessModes: ["ReadWriteOnce"]
    resources:
      requests:
        storage: 1Gi
```

- **Purpose: volumeClaimTemplates** is a feature of StatefulSets that allows for the automatic creation of Persistent Volume Claims (PVCs) for each Pod in the StatefulSet.
- **metadata**: name: data: This names the PVC. Each Pod in the StatefulSet will have its own PVC created with this name, suffixed by the Pod's ordinal index.
- **accessModes: ["ReadWriteOnce"]**: Specifies how the volume can be accessed. ReadWriteOnce means the volume can be mounted for read-write by a single node. This is typical for databases where you want one instance (Pod) to have exclusive access to the volume.
- **resources: requests: storage: 1Gi**: This requests a storage capacity of 1 Gigabyte for the PVC. It specifies the minimum size of the PV that should be provisioned to back this PVC.

How They Work Together

- When the StatefulSet is deployed, Kubernetes automatically creates a PVC for each Pod using the template defined in `volumeClaimTemplates`.
- Each PVC is then bound to a Persistent Volume (PV) either dynamically (created on-demand by the storage provisioner) or by manually binding to an existing PV, depending on your cluster's storage setup.
- Inside each Pod, the volume is mounted at the specified `mountPath`, making the data stored by PostgreSQL persistent across Pod restarts and rescheduling.

## Liveness vs Readiness Probes

- **Liveness Probes**: Check if your application is running. If a liveness probe fails, Kubernetes will restart the container.
- **Readiness Probes**: Check if your application is ready to serve traffic. If a readiness probe fails, Kubernetes stops sending traffic to the pod until it passes.

## Importance in PostgreSQL Context

- **Liveness Probe**: Ensures that if PostgreSQL becomes unresponsive (deadlocked or crashed), the container is restarted to bring the database back to a healthy state.
- **Readiness Probe**: Ensures traffic is only sent to the pod once PostgreSQL is ready to handle it, preventing premature traffic routing that could result in failed database connections.

In summary, these probes are crucial for maintaining the health, reliability, and availability of your PostgreSQL instances within Kubernetes. They help the system make intelligent decisions about managing and routing traffic to your database pods.

### DNS Lookup Command

To see the DNS entries for the pods in the headless service:

```bash
kubectl run -i --tty --rm debug --image=andreswebs/postgresql-client --restart=Never --env="PGPASSWORD=postgres" -- psql -h postgres.default.svc.cluster.local -U postgres -d postgres
```

### Pod Identity

```bash
kubectl logs postgres-0 -c postgres-init
kubectl logs postgres-1 -c postgres-init
am a read-only replica
```

## Add replicas

```bash
kubectl scale sts postgres --replicas 5
kubectl scale sts postgres --replicas 2
```
