const express = require('express');
const app = express();
const port = 3000;

const welcomeMessage = process.env.WELCOME_MESSAGE || 'Bienvenue sur notre application!';

app.get('/', (req, res) => {
  res.send(`<h1>${welcomeMessage}</h1>`);
});

app.listen(port, () => {
  console.log(`Application démarrée sur http://localhost:${port}`);
});
