# Kubernetes ConfigMaps Tutorial

This tutorial will guide you through the process of creating and managing ConfigMaps in Kubernetes. ConfigMaps are key resources for separating configuration artifacts from image content, enhancing the portability of containerized applications. We'll explore how to create, use, and manage ConfigMaps effectively.

## Creating a ConfigMap

### 1. Creating a ConfigMap from Literal Values

Command:

```bash
kubectl create configmap my-config --from-literal=key1=value1 --from-literal=key2=value2
```

**Use Case**: When you have a few simple key-value pairs to store.

### 2. Creating a ConfigMap from a File

Command:

```bash
kubectl create configmap my-config --from-file=path/to/config/file
```

**Use Case**: When you want to store the contents of a file in a ConfigMap.

### 3. Creating a ConfigMap from a Directory of Files

Command:

```bash
kubectl create configmap my-config --from-file=path/to/directory
```

**Use Case**: Use Case: When you have multiple configuration files that you want to store in a single ConfigMap.

## Using ConfigMaps in Pods

### Setting Environment Variables from a ConfigMap

- **Pod YAML Example**:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
  - name: mycontainer
    image: myimage
    envFrom:
    - configMapRef:
        name: my-config
```

**Use Case**: To set environment variables in a container from a ConfigMap.

### Mounting a ConfigMap as a Volume

- **Pod YAML Example**:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
  - name: mycontainer
    image: myimage
    volumeMounts:
    - name: config-volume
      mountPath: /etc/config
  volumes:
  - name: config-volume
    configMap:
      name: my-config
```

**Use Case**: To mount a ConfigMap as a volume in a Pod, making the configuration data stored in the ConfigMap available as files.

### Updating ConfigMaps

- **Edit a ConfigMap**:

```bash
kubectl edit configmap my-config
```

**Note**: Changes to a ConfigMap are not automatically propagated to Pods. You need to recreate the Pods to pick up the updated ConfigMap.

### Deleting ConfigMaps

- **Command**:

```bash
kubectl delete configmap my-config
```

**Use Case**: When you no longer need the ConfigMap.
