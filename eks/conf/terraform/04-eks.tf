resource "aws_eks_cluster" "eks-cluster" {
  name     = "my-cluster"
  role_arn = var.iam_role

  vpc_config {
    subnet_ids = module.vpc.public_subnets
  }
}

resource "aws_eks_node_group" "worker-node-group" {
  cluster_name    = aws_eks_cluster.eks-cluster.name
  node_group_name = "workshop-workernodes"
  node_role_arn   = var.iam_role
  subnet_ids      = module.vpc.public_subnets
  instance_types  = ["t3.large"]

  remote_access {
    ec2_ssh_key               = var.ec2_key
    source_security_group_ids = [aws_security_group.eks_sg.id]
  }

  scaling_config {
    desired_size = 2
    max_size     = 2
    min_size     = 2
  }

  tags = {
    Name      = "EKS Node",
    Terraform = "True"
  }

}

resource "aws_eks_addon" "vpc-cni" {
  cluster_name = aws_eks_cluster.eks-cluster.name
  addon_name   = "vpc-cni"
}

resource "aws_eks_addon" "core-dns" {
  cluster_name = aws_eks_cluster.eks-cluster.name
  addon_name   = "coredns"
}

resource "aws_eks_addon" "ebs-csi" {
  cluster_name = aws_eks_cluster.eks-cluster.name
  addon_name   = "aws-ebs-csi-driver"
}


resource "aws_eks_addon" "kube-proxy" {
  cluster_name = aws_eks_cluster.eks-cluster.name
  addon_name   = "kube-proxy"
}

##### ASG

# resource "aws_launch_template" "eks_nodes" {
#   name_prefix   = "eks-nodes-"
#   image_id      = data.aws_ami.eks_optimized.id
#   instance_type = "t3.large"

#   vpc_security_group_ids = [aws_security_group.eks_sg.id]

#   iam_instance_profile {
#     name = "LabInstanceProfile"

#   }

#   user_data = base64encode(<<-EOT
#     #!/bin/bash
#     /etc/eks/bootstrap.sh ${aws_eks_cluster.eks-cluster.id} --kubelet-extra-args '--node-labels=asg=true,epita=yes'
#     EOT
#   )

#   tag_specifications {
#     resource_type = "instance"
#     tags = {
#       Name = "EKS Node"
#     }
#   }
# }

# resource "aws_autoscaling_group" "eks_asg" {
#   desired_capacity    = 2
#   max_size            = 3
#   min_size            = 1
#   vpc_zone_identifier = module.vpc.public_subnets

#   launch_template {
#     id      = aws_launch_template.eks_nodes.id
#     version = "$Latest"
#   }

#   tag {
#     key                 = "Name"
#     value               = "EKS Node"
#     propagate_at_launch = true
#   }
# }
