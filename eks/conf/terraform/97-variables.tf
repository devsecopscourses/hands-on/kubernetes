# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}

variable "iam_role" {
  description = "IAM Role"
  type        = string
  default     = "arn:aws:iam::812291240755:role/LabRole"
}

variable "ec2_key" {
  description = "EC2 Key"
  type        = string
  default     = "vockey"
}
