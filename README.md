# Kubernetes

**Kubernetes**, often abbreviated as **K8s**, is a powerful open-source system for container management, such as Docker. It automates the deployment, scaling, and operation of containerized applications. By centralizing configuration and maintenance, Kubernetes facilitates high availability, resilience, and scalability of applications. It manages containers across various hosts and supports load balancing and service discovery, making the application infrastructure more flexible and efficient. Ideal for cloud environments, Kubernetes has become the go-to solution for large-scale container orchestration.

You can go further and do [EKS Workshop](https://www.eksworkshop.com/docs/introduction)

## Basic Architecture of Kubernetes

In this section, we'll delve into the core components of Kubernetes, understanding how they work together to manage containerized applications efficiently.

### Nodes and Pods

In Kubernetes, the architecture is built around Nodes and Pods, each serving a distinct role in the ecosystem.

#### Nodes

- **Definition**: A Node is a worker machine in Kubernetes. It can be either a physical machine in a data center or a virtual machine in the cloud. Nodes are the backbone of a Kubernetes cluster, providing the runtime environment for Pods.
- **Components**: Each Node is equipped with the essential components to run Pods, including the Kubelet, a Kube-proxy, and a container runtime like Docker.
- **Management**: Nodes are managed by the Control Plane components. The Control Plane's responsibility includes scheduling Pods onto Nodes, monitoring Nodes, and handling node failures.

#### Pods

- **Fundamental Unit**: Pods are the smallest deployable units in Kubernetes. Each Pod represents a single instance of a running process in your cluster.
- **Container Grouping**: A Pod encapsulates one or more containers, typically Docker containers. These containers within a Pod share the same network space, IP address, and port space, and can find each other via localhost. They also share storage, as defined by Volumes.
- **Lifecycle and Management**: Pods are ephemeral in nature. They are created, assigned to Nodes, and then terminated as per the cluster's needs. Kubernetes automates the deployment and scaling of Pods.

![Node and Pod Schema](images/node-and-pod.png)

#### Pod States in Kubernetes

Understanding the state of a Pod is crucial for managing and troubleshooting Kubernetes applications. A Pod's state reflects its current lifecycle status.

Pod Lifecycle States

- **Pending**: The Pod has been accepted by the Kubernetes system, but one or more of the container images has not been created. This includes time before being scheduled as well as time spent downloading images over the network, which could take a while.
- **Running**: The Pod has been bound to a Node, all of the containers have been created, and at least one container is still running, or is starting or restarting.
- **Succeeded**: All containers in the Pod have terminated successfully, and will not be restarted.
- **Failed**: All containers in the Pod have terminated, and at least one container has terminated in a failure (exited with a non-zero exit code or was stopped by the system).
- **Unknown**: For some reason the state of the Pod could not be obtained, typically due to an error in communicating with the host of the Pod.

Pod Conditions

Pods also report an array of conditions that may indicate transitional or terminal states. Common conditions include:

- **PodScheduled**: Indicates if the Pod has been scheduled to a node.
- **ContainersReady**: Indicates whether all containers in the Pod are ready.
- **Initialized**: Indicates whether all init containers have started successfully.
- **Ready**: Indicates whether the Pod is able to serve requests and should be added to the load balancing pools of all matching services.

![Pod States](images/podstate.png)

## Control Plane

The Control Plane is the core of Kubernetes' functionality, making global decisions about the cluster and responding to cluster events.

- **kube-apiserver**: The API server is a component of the Kubernetes control plane that exposes the Kubernetes API.
- **etcd**: Reliable and consistent storage for all cluster data.
- **kube-scheduler**: Watches for newly created Pods with no assigned Node, and selects a Node for them to run on.
- **kube-controller-manager**: Runs controller processes, which are background threads that handle routine tasks in the cluster.
- **cloud-controller-manager**: Lets you link your cluster into your cloud provider's API.

In a cloud environment, Kubernetes' Control Plane is often fully managed, meaning the cloud provider handles the maintenance and availability of these core components. This management significantly reduces the complexity and overhead of running a Kubernetes cluster, allowing teams to focus more on deploying and managing their applications rather than the underlying infrastructure.

![Control Plane](images/kubernetes-components.svg)

## Node Components

While the Control Plane manages the cluster globally, Node Components run on every node, maintaining running pods and providing the Kubernetes runtime environment.

### kubelet

The kubelet runs on every node in the cluster. It's a primary node agent that ensures containers are running in a Pod. The kubelet takes PodSpecs and ensures that the containers described in these PodSpecs are running and healthy. It interacts with both the container runtime and the underlying host system, providing information on Pod and node status back to the Control Plane.

### kube-proxy

kube-proxy is a network proxy that runs on each node in your Kubernetes cluster. It maintains network rules on nodes, allowing network communication to your Pods from inside or outside of your cluster. kube-proxy manages IP and port number routing of incoming requests to the correct container, ensuring the predictability and accessibility of the networking environment.

## Kubectl basics

You can create and manage Kubernetes resources by using the Kubernetes command line interface, **kubectl**. Kubectl uses the Kubernetes API to interact with the cluster

The common format of a kubectl command is: `kubectl action resource`

This performs the specified action (like *create*, *describe* or *delete*) on the specified *resource* (like *node* or *deployment*). You can use --help after the subcommand to get additional info about possible parameters (for example: kubectl get nodes --help).

Check that kubectl is configured to talk to your cluster, by running the `kubectl version` command.

Check that kubectl is installed and you can see both the client and the server versions.

### Here are some commands for nodes

- **List all Nodes in the cluster:**
  
```bash
kubectl get nodes
```

- **Get detailed information about a Node:**
  
```bash
kubectl describe node [node_name]
```

- **Mark a Node for maintenance (drain):**
  
```bash
kubectl drain [node_name]
```

- **Mark a Node as schedulable again (uncordon):**
  
```bash
kubectl uncordon [node_name]
```

- **Delete a Node from the cluster:**
  
```bash
kubectl delete node [node_name]
```

### Here are some commands for pods

- **List all Pods:**
  
```bash
kubectl get pods
```

- **Get detailed information about a specific Pod:**
  
```bash
kubectl describe pod [pod_name]
```

- **Create a Pod using a YAML file:**
  
```bash
kubectl create -f [file.yaml]
```

- **Delete a Pod:**
  
```bash
kubectl delete pod [pod_name]
```

- **Execute a command in a container within a Pod:**
  
```bash
kubectl exec [pod_name] -- [command]
```

![kubecheatsheet](images/kubecheatsheet.png)

## Kubernetes Namespaces

Kubernetes namespaces are used to segment a cluster's resources between multiple users and projects. They provide a logical organization of components and can assist in managing environments with multiple teams or projects.

### Isolation

- **Purpose**: Namespaces allow for the isolation of resources within a cluster.
- **Functionality**: Each namespace can contain its own services, pods, volumes, etc.

### Resource Quotas

**Management**: Administrators can assign resource quotas per namespace, thus controlling the resource usage of the cluster in a granular way.

### Deployment Simplification

**Use Case**: Namespaces enable the deployment of applications in distinct environments (such as development, testing, and production) within the same cluster.

### kubectl Commands for Namespaces

- **List all namespaces:**

```bash
kubectl get namespaces
```

- **Create a new namespace:**

```bash
kubectl create namespace [namespace_name]
```

- **List resources in a specific namespace:**

```bash
kubectl get pods --namespace [namespace_name]
```

- **Set a default namespace for kubectl commands:**

```bash
kubectl config set-context --current --namespace=[namespace_name]
```

- **Delete a namespace:**

```bash
kubectl delete namespace [namespace_name]
```

### Using Namespaces for Pods and Nodes

When working with pods and nodes, you can specify the namespace to target your actions on the resources of that specific namespace.

- **List all Pods in a specific namespace:**

```bash
kubectl get pods --namespace [namespace_name]
```

- **Describe a Pod in a specific namespace:**

```bash
kubectl describe pod [pod_name] --namespace [namespace_name]
```

Note that nodes are not specific to a namespace. When working with nodes, you interact with resources at the entire cluster level.

Namespaces are a powerful tool for resource management and segmentation within a Kubernetes cluster, allowing for better organization and security.

## Kubernetes Deployments

Deployments in Kubernetes are crucial for managing stateless applications, providing powerful features like updating and scaling. Understanding Deployments helps you ensure that your application runs as intended and is available to serve user requests.

### What is a Deployment?

- **Purpose**: A Deployment provides declarative updates for Pods and ReplicaSets. You describe a desired state in a Deployment, and the Deployment Controller changes the actual state to the desired state at a controlled rate.
- **Use Cases**: Deployments are used to create new ReplicaSets, or to remove existing Deployments and adopt all their resources with new Deployments.

### ReplicaSets

A ReplicaSet is an integral part of a Deployment. It ensures that a specified number of pod replicas are running at any given time.

- **Function**: The ReplicaSet creates and destroys Pods as needed to reach the desired state. If a Pod goes down, the ReplicaSet replaces it.
- **Relation to Deployment**: Deployments manage ReplicaSets and provide the mechanism to update the application and its configuration over time.

### Features of Deployments

1. **Automated Rollouts and Rollbacks**: Deployments can create new ReplicaSets, or they can remove existing Deployments and adopt all their resources with new Deployments.
2. **Scaling**: Deployments can be scaled up or down by updating the number of replicas.
3. **Self-Healing**: Deployments ensure that your application can recover from failure scenarios (like node failures) by automatically replacing failed Pods.
4. **Update Strategy**: You can define strategies for updates, like Rolling Update or Recreate.

![deployment-replicaset](images/deployment-replicaset.png)

# Kubernetes Services

Kubernetes Services are an abstract way to expose an application running on a set of Pods as a network service. With Kubernetes, you don't need to modify your application to use an unfamiliar service discovery mechanism. Kubernetes gives Pods their own IP addresses and a single DNS name for a set of Pods and can load-balance across them.

## Types of Kubernetes Services

### ClusterIP

- **Default Service Type**: ClusterIP is the default Kubernetes service. It gives a service an internal IP address that is used to communicate within the cluster.
- **Use Case**: Ideal for internal communication between services within the cluster.
- **Accessibility**: Only accessible within the cluster, not from the external environment.

![clusterIP](images/clusterIP.png)

### NodePort

- **External Accessibility**: NodePort exposes the service on each Node’s IP at a static port (the NodePort). A ClusterIP service, to which the NodePort service routes, is automatically created.
- **Port Range**: You can access the NodePort service, from outside the cluster, by requesting `<NodeIP>:<NodePort>`.
- **Use Case**: Useful for external access to your services, like web applications.

![nodePort](images/nodePort.png)

### LoadBalancer

- **Cloud Integration**: LoadBalancer exposes the service externally using a cloud provider’s load balancer.
- **Automatic Creation**: It assigns a fixed, external IP address to the service. In the case of cloud environments, the LoadBalancer type automatically creates the corresponding load balancer in the cloud provider’s environment.
- **Use Case**: Ideal for production environments for distributing traffic across multiple nodes or Pods.

### ExternalName

- **CNAME Record**: ExternalName maps the service to the contents of the `externalName` field (e.g., `foo.bar.example.com`), by returning a CNAME record with its value. No proxying of any kind is set up.
- **Use Case**: Useful when you want to access an external service that is not part of your cluster.

## Kubernetes Persistent Volumes (PV) and Persistent Volume Claims (PVC)

Kubernetes offers robust solutions for managing persistent storage through Persistent Volumes (PV) and Persistent Volume Claims (PVC), coupled with the concept of Storage Classes for dynamic provisioning.

### Persistent Volumes (PV)

- **Definition**: A Persistent Volume in Kubernetes is a cluster-level storage resource provisioned by an administrator or dynamically through Storage Classes.
- **Characteristics**: Unlike regular volumes, a PV has a lifecycle independent of any individual pod that uses it. It represents an abstraction of storage resources (like NFS, iSCSI, or cloud-provider-specific systems).
- **Management**: PVs are managed at the cluster level, not at the individual pod level.

### Access Modes

PVs in Kubernetes support different access modes to address various use cases:

1. **ReadWriteOnce (RWO)**:
   - **Definition**: This mode allows the volume to be mounted as read-write by a single node.
   - **Use Case**: Ideal for single-user applications like databases.
   - **Limitation**: Only one node can mount the volume in read-write mode at a time.

2. **ReadOnlyMany (ROX)**:
   - **Definition**: This mode allows the volume to be mounted as read-only by multiple nodes.
   - **Use Case**: Suitable for scenarios where multiple pods need to read from the same volume, like web servers serving static content.
   - **Limitation**: Write operations are not allowed; the volume is read-only.

3. **ReadWriteMany (RWX)**:
   - **Definition**: This mode allows the volume to be mounted as read-write by multiple nodes.
   - **Use Case**: Used in applications requiring shared access, like file sharing apps or data analysis software.
   - **Limitation**: Limited support by storage backends; not all storage systems can handle concurrent read-write access efficiently.

### Reclaim Policies in Kubernetes

Kubernetes supports several Reclaim Policies for PVs:

1. **Retain**:
   - **Definition**: PV remains in the cluster but is released from its claim when the PVC is deleted.
   - **Use Case**: Ideal for data preservation. Requires manual recovery of data and management of the volume post-release.
   - **Management**: Administrator intervention required for reclamation.

2. **Delete**:
   - **Definition**: PV and the underlying storage resource are automatically deleted when the PVC is deleted.
   - **Use Case**: Useful for automatic cleanup of resources, especially in dynamic provisioning scenarios.
   - **Limitation**: Data loss occurs once the PVC is deleted.

![accessmodes](images/accessmode.jpg)

### Persistent Volume Claims (PVC)

- **Definition**: A PVC is a request for storage by a user or pod within Kubernetes.
- **Functionality**: PVCs allow users to request specific sizes and access modes (such as ReadWriteOnce, ReadOnlyMany, ReadWriteMany).
- **PV-PVC Binding**: When a PVC is created, it is automatically bound to an available PV that meets its requirements.

### Storage Classes

- **Role**: Storage Classes define categories of storage with distinct provisioning parameters.
- **Dynamic Provisioning**: They enable the dynamic provisioning of PVs. If no existing PV matches the requirements of a PVC, a new PV is automatically created using the appropriate Storage Class.
- **Customization**: Administrators can define multiple Storage Classes for different types of storage (e.g., various performance types or backup policies).

### Utilization of PV and PVC

PV and PVC are critical for managing persistent storage in Kubernetes applications, particularly for data that must survive pod destruction and recreation, like databases. They provide an abstraction layer that allows developers to utilize storage resources without concern for the specific underlying infrastructure details.

### Conclusion

In summary, PVs and PVCs in Kubernetes provide a robust system for managing persistent storage, while Storage Classes allow for flexible and dynamic storage provisioning management. This ensures that applications can have efficient and reliable data persistence, which is crucial for many modern production environments.

![pv-pvc](images/pv-pvc.png)

![pv-pvc-sc](images/pv-pvc-sc.jpg)

## Kubernetes ConfigMaps

ConfigMaps in Kubernetes are a key resource used to separate configuration artifacts from image content to keep containerized applications portable. They provide a way to store configuration data as key-value pairs and can be used to store fine-grained information like individual properties or coarse-grained information like entire configuration files or JSON blobs.

### Understanding ConfigMaps

ConfigMaps allow you to decouple configuration details from your application code. This makes your application easier to configure without the need to rebuild your application images, and it helps in keeping containerized applications portable across different environments.

### Use Cases for ConfigMaps

1. **Storing Configuration Files**: You can store entire configuration files in a ConfigMap and mount them into your Pods.
2. **Setting Environment Variables**: ConfigMaps can be used to set environment variables for a container in a Pod.
3. **Command-line Arguments**: You can use ConfigMaps to define command-line arguments for your application.
4. **Populating Volume with Data**: ConfigMaps can be used to populate a volume with data, which can then be accessed by containers in a Pod.

### Best Practices

1. **Avoid Sensitive Data**: Do not store sensitive data in ConfigMaps. Use Secrets for that purpose.
2. **Immutable ConfigMaps**: Treat ConfigMaps as immutable. If you need to update the configuration, create a new ConfigMap and update the references in your deployments.
3. **Versioning**: Include a version number or a hash in the ConfigMap name to facilitate rolling updates and rollbacks.

ConfigMaps are a versatile tool in Kubernetes, enabling efficient configuration management and helping to keep applications portable and easy to manage across different environments.

## Kubernetes StatefulSets

StatefulSets are a Kubernetes resource used to manage stateful applications. They provide unique identities to each pod they manage and maintain the order and uniqueness of these pods.

### Understanding StatefulSets

- **Stable Identity**: Each pod in a StatefulSet derives its hostname from the name of the StatefulSet and a unique ordinal index. This ensures that each pod can be easily identified and communicated with.
- **Ordered Deployment**: Pods are created sequentially in the order of their ordinal indices.
- **Ordered Termination**: When deleting pods, they are terminated in reverse ordinal order.
- **Persistent Storage**: StatefulSets can use Persistent Volumes to provide stable and durable storage for stateful applications.

### Use Cases for StatefulSets

- **Databases**: Ideal for running databases like MySQL, PostgreSQL, or MongoDB that require stable, persistent storage, and unique network identifiers.
- **Clustered Applications**: Applications that require a fixed network identity and stable storage, like Apache ZooKeeper or etcd.

### StatefulSet Components

A typical StatefulSet definition includes:

- The Pod template to define the containers and volumes.
- The volumeClaimTemplates to provide stable storage using Persistent Volumes.

![statefulset](images/statefulset_pvc.png)

# Helm Overview

## What is Helm?

Helm is a package manager for Kubernetes, facilitating the installation and management of Kubernetes applications. It uses packages called "charts" to streamline the deployment of complex applications.

## Installation

Install Helm by downloading the appropriate version from the [Helm GitHub project](https://github.com/helm/helm/releases), then unpack and add it to your PATH.

```bash
helm init
```

# Using Helm: Basic Commands

## Adding Repositories

Add new chart repositories to Helm:

```bash
helm repo add [repo name] [repo URL]
```

## Adding Repositories

Update your local Helm chart repository cache:

```bash
helm repo update
```

## Searching for Charts

Search for charts in your Helm repositories:

```bash
helm search repo [chart name]
```

## Installing Charts

Install a chart from a repository:

```bash
helm install [release name] [chart name]
```

## Upgrading Releases

Upgrade an existing release:

```bash
helm upgrade [release name] [chart name]
```

## Rolling Back Releases

Roll back a release to a previous version:

```bash
helm rollback [release name] [revision]
```

## Uninstalling Releases

Remove a deployed release:

```bash
helm uninstall [release name]
```

## Listing Releases

List all deployed releases in a cluster:

```bash
helm list
```
