# Minikube

Minikube is a tool that creates a local Kubernetes cluster. The installation steps vary slightly depending on your operating system. Find more information about the installation [here](https://kubernetes.io/fr/docs/tasks/tools/install-minikube/)

# Minikube Basics Commands

Minikube is a popular tool for creating a local Kubernetes cluster, ideal for learning and development purposes. Below are some basic commands for using Minikube:

## Starting Minikube

To start a Minikube cluster, use the following command:

```bash
minikube start
```

## Accessing Kubernetes Dashboard

Minikube includes a web-based dashboard for Kubernetes, providing a user interface to manage your cluster. To access it, use:

```bash
minikube dashboard  
```

Note: If minikube doesn't open a web browser, you can use the `--url` flag.

## Enabling Addons

Minikube comes with several addons that you can enable or disable. For example, to enable the `metrics-server` addon:

```bash
minikube addons enable metrics-server
```

```bash
minikube addons disable metrics-server
```

## Getting Minikube IP Address

To get the IP address of the Minikube cluster:

```bash
minikube ip
```
